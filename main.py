import python.getdata as getdata
import pandas as pd
import sys
import networkx as ns
import python.plotting as plot
import os
import time
import python.htmlbuild as htmlbuild
from argparse import ArgumentParser
from collections import Counter
import copy
from python.kml_import import plekken_add_coordinates

def parse_arguments():
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--saved", action="store_true", help="Gebruik de huidige `sheet_data` pickle file ipv de data te updaten.")
    arg_parser.add_argument("--render-only", action="store_true", help="Alleen de webpagina updaten, geen plots genereren.")
    arg_parser.add_argument("--kml-file", type=str, default="Vlaggenroof Stanleystam Maart 2021.kml", help="Data inladen van een KML bestand.")
    return arg_parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()

    if not args.render_only:
        # data binnenhalen met gekozen methode
        if args.saved:
            plekken = getdata.saved()
        else:
            plekken = getdata.update()
        plekken = plekken_add_coordinates(plekken, args.kml_file)

        print(plekken)

        verstoppers = plekken.verstopper.unique()

        vinders = plekken.vinder.unique()

        dagen = pd.date_range(plekken.loc[0, 'verstopdag'],
                            plekken.tail(1).iloc[0]['vinddag'], freq='1D')

        uiterste_tijden = (plekken.loc[0, 'vindtijd'],
                        plekken.tail(1).iloc[0]['vindtijd'])

        overdag = plekken[plekken.nacht==False]
        
        def puntenaantal_geschiedenis_gen(plekken):
            puntenaantal_geschiedenis = []
            counter = Counter()
            for vinder in plekken.vinder:
                counter[vinder] += 1
                puntenaantal_geschiedenis.append(copy.copy(counter))
            return puntenaantal_geschiedenis

        puntenaantal_geschiedenis = puntenaantal_geschiedenis_gen(plekken)

        # lekker plotten
        plot.punten(plekken, vinders)
        plot.taart(plekken, vinders)
        plot.dagen(plekken, dagen)
        plot.punten_per_dag(plekken, vinders, dagen)
        plot.punten_per_vinder_per_dag(plekken, vinders, dagen)
        plot.tijdshistogram(uiterste_tijden, plekken, bins=15)
        plot.hoe_lang_ligt_de_vlag(plekken, overdag)
        plot.hoe_lang_duurt_verstoppen(plekken)
        plot.populaire_vindtijden(plekken)
        plot.gemiddelde_verstoptijd(plekken, verstoppers)
        plot.populaire_vindtijden(plekken)
        plot.gemiddelde_verstoptijd(plekken, verstoppers)
        plot.gemiddelde_vindtijd(overdag, vinders)
        plot.scatter_vind_verstop(overdag)
        plot.wiens_vlag_gepakt(plekken, vinders, verstoppers)
        #plot.graaf(plekken, vinders)
        #plot.vlagverloop(plekken, vinders)
        plot.hoe_lang_verstopt_hist(plekken)
        plot.tijdlijn(plekken, verstoppers)
        plot.puntenverdeling_over_tijd(plekken, puntenaantal_geschiedenis,
                                       vinders)
        plot.puntenverdeling_over_tijd(plekken, puntenaantal_geschiedenis,
                                       vinders, relatief=False)
        plot.gemiddelde_afstand_afgelegd(plekken, verstoppers)
        plot.totale_afstand_afgelegd(plekken, verstoppers)
        plot.gemiddelde_snelheid(plekken, verstoppers)
        plot.scatter_verband_verstop_tijd_afstand(plekken)
    tijd = time.strftime('%Y-%m-%d, %H:%M')
    with open('public/index.html', 'w') as file:
        file.write(htmlbuild.build(tijd))
    
    print('geupdate website staat klaar voor push, timestamp: '+tijd)