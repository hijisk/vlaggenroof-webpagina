from jinja2 import Template
import os

def build(time):
  all_plots = os.listdir('public/plots/')
  top_plots = [
    ("Huidige totaalscores", "taartpunten.png"),
    ("Puntenverdeling", "punten.png"),
    ("Locaties per dag", "dagen.png"),
    ("Vlaggen vinden", "gemiddelde_vindtijd.png"),
    ("Vlaggen verstoppen", "gemiddelde_verstoptijd.png"),
    ("Gemiddelde afstanden", "gemiddelde_afstand_afgelegd.png"),
    ("Totale afstanden", "totale_afstand_afgelegd.png"),
    ("Snelle &amp; langzame verstoppers", "gemiddelde_snelheid.png")
  ]

  for img in [plot[1] for plot in top_plots]:
    all_plots.remove(img)

  with open('template.html', 'r') as template:
    return Template(template.read()).render({
      "last_update": time, "top_plots": top_plots, "plots": all_plots
    })
