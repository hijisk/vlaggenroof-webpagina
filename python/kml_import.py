import xml.etree.ElementTree as ET
import pandas as pd
from geopy.distance import distance

first_pin = 3

def import_kml(path):
  tree = ET.parse(path)
  root = tree.getroot()

  mainfolder = None
  for i in list(root)[0]:
      if i.tag == '{http://www.opengis.net/kml/2.2}Folder':
          mainfolder = i
          break

  if mainfolder is None:
      print("Did not find main folder. Abort.")
      exit()

  pins = {}
  for loc in mainfolder:
      if loc.tag == '{http://www.opengis.net/kml/2.2}Placemark':
          pin = {}
          for att in loc:
            if att.tag == '{http://www.opengis.net/kml/2.2}Point':
              for x in att:
                if x.tag == '{http://www.opengis.net/kml/2.2}coordinates':
                  pin["coords"] = [float(c) for c in x.text.split(',')[:2]]
            if att.tag == '{http://www.opengis.net/kml/2.2}name':
              pin["n"] = int(att.text)
          if "n" in pin and "coords" in pin:
            pins[pin["n"]] = pin["coords"]
  return pins

def plekken_add_coordinates(df, path):
  pins = import_kml(path)
  for index, row in df.iterrows():
    if index+first_pin in pins:
      row['long'] = pins[index+first_pin][0]
      row['lat'] = pins[index+first_pin][1]
      if index == 0 or 'lat' not in plekken.iloc[index-1] or 'long' not in plekken.iloc[index-1]:
        row['afstand_afgelegd'] = 0
      else:
        row['afstand_afgelegd'] = distance((row['lat'], row['long']), (plekken.iloc[index-1]['lat'], plekken.iloc[index-1]['long'])).m
    if index == 0:
        plekken = pd.DataFrame(row).transpose()
    else:
      plekken = plekken.append(row)
  return plekken