from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import gspread
import pickle
import numpy as np
from datetime import date, time, datetime, timedelta
from collections import Counter, UserList, UserDict


def plekken_add_columns(df):
    #
    for index, row in df.iterrows():
        row['hoe_laat_gevonden'] = datetime.strptime(row['vindtijd'], '%H:%M').time()
        row['hoe_laat_verstopt'] = datetime.strptime(row['verstoptijd'], '%H:%M').time()
        row['hoe_laat_gevonden_m'] = row['hoe_laat_gevonden'].hour+row['hoe_laat_gevonden'].minute/60
        row['hoe_laat_verstopt_m'] = row['hoe_laat_verstopt'].hour+row['hoe_laat_verstopt'].minute/60
        row['nacht'] = False if row['vinddag'] == row['verstopdag'] else True
        row['vinddag'] = datetime.strptime(row['vinddag'], '%d-%m').date().replace(year=2021)
        row['verstoptijd'] = datetime.strptime(row['verstopdag'] + '-' + row['verstoptijd'], '%d-%m-%H:%M').replace(year=2021)
        row['vindtijd'] = datetime.strptime(row['verstopdag'] + '-' + row['vindtijd'], '%d-%m-%H:%M').replace(year=2021)+timedelta(row['nacht'])
        row['verstopdag'] = datetime.strptime(row['verstopdag'], '%d-%m').date().replace(year=2021)
        row['dag_delta'] = (row['vinddag'] - row['verstopdag']).days
        row['vind_duur'] = (row['vindtijd'] - row['verstoptijd']).seconds//60
        if index ==0:
            row['verstop_duur'] = 0
            plekken = pd.DataFrame(row).transpose()
        else:
            row['verstop_duur'] = (row['verstoptijd'] - plekken.iloc[index-1]['vindtijd']).seconds//60
            plekken = plekken.append(row)
    return plekken

def saved():
    #get data from the saved sheet_data
    with open('python/sheet_data', 'rb') as file:
        data = pickle.load(file)
    return data

def update():
    #updates the sheet_data file with the information in the google sheet
    scope = [
    'https://www.googleapis.com/auth/spreadsheets',
    ]

    GOOGLE_SHEETS_KEY_FILE = 'python/formidable-app-240111-f4165c7b96b0.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(GOOGLE_SHEETS_KEY_FILE, scope)
    gc = gspread.authorize(credentials)

    sheet_id = "1W0Pm_jE0e5pKRzSprZz2aneY4EnmwNO3EMnH014ROiE"
    workbook = gc.open_by_key(sheet_id)
    sheet = workbook.get_worksheet(0)
    values = sheet.get_all_values()
    data = pd.DataFrame(values[1:], columns=values[0])
    data.replace("", np.nan, inplace=True)
    data.dropna(inplace=True)

    data = plekken_add_columns(data)
    print(data)
    with open('sheet_data', 'wb') as file:
        pickle.dump(data, file)
    print('The above dataframe is now in sheet_data')
    return data


class Vlag(UserList):
    def __init__(self, vorige_vlag):
        if vorige_vlag == False:
            vlag_index=0
        

class Verstopplek():

    def vind_duur(self):
        return (self.vindtijd - self.verstoptijd).seconds//60


    def __init__(self, plek, verstopper, hoe_laat_verstopt, vinder,
                 hoe_laat_gevonden, verstopdag, vorige_plek, eind, vlag):
        self.plek = plek
        self.verstopper = verstopper
        self.hl_verstopt = datetime.strptime(hoe_laat_verstopt, '%H:%M').time()
        self.vinder = vinder
        self.hl_gevonden = datetime.strptime(hoe_laat_gevonden, '%H:%M').time()

        self.verstopdag = datetime.strptime(verstopdag,
                                            '%d-%m').date().replace(year=2021)
        self.vlag = vlag

        if vorige_plek is False:
            self.begin = True
            self.index = 0
        else:
            self.index = vorige_plek.index + 1
        if eind is True:
            self.eind = True
    
    def volgende(self):
        return vlag[index+1]

    def vorige(self):
        return vlag[index-1]

    def hl_gevonden_h(self):
        return self.hl_gevonden.hour+self.hl_gevonden.minute/60

    def hl_verstopt_h(self):
        return self.hl_verstopt.hour+self.hl_verstopt.minute/60

    def plek(self):
        return self.plek
    
    def verstopper(self):
        return self.verstopper

    def vinder(self):
        return self.vinder
    
    def hl_verstopt(self):
        return self.hl_verstopt

    def hl_gevonden(self):
        return self.hl_gevonden

    def vorige(self):
        return self.vorige
    
    def __add__(self, integer):
        obj = self
        for i in range(integer):
            obj = obj.volgende()