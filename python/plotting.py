import matplotlib.pyplot as plt
import matplotlib
import math
import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.dates as mdates
from matplotlib.patches import Rectangle, Patch
from matplotlib.lines import Line2D
import datetime
import time
path = 'public/plots/'


bewaar_kleuren = []

def kleuren(verstoppers):
    if bewaar_kleuren == []:
        bewaar_kleuren = plt.cm.get_cmap('tab20', len(verstoppers))
    return bewaar_kleuren

def num_to_text(minutes):
    if minutes < 60:
        return '{} min'.format(minutes)
    if minutes >= 60:
        hours = minutes // 60
        rest_minutes = minutes % 60
        return_string = '{}:{} h'.format if rest_minutes else '{} h' .format
        return return_string(hours, rest_minutes)


def date_ticker(ax, string):
    ax.xaxis.set_major_formatter(mdates.DateFormatter(string))
    ax.xaxis.set_major_locator(mdates.DayLocator())


def make_subplots_for(lijst):
    kolommen = 4
    rijen = math.ceil(len(lijst)/4)
    fig, axs = plt.subplots(
        rijen, kolommen, sharex=False, sharey=True, squeeze=False,
        figsize=[3.9*kolommen, 3.9*rijen])
    for ax in axs.flatten():
        ax.axis('off')
    return fig, axs


def avondklok_intekenen(ax):
    avondklok_begin = list(pd.date_range(datetime.datetime(2021, 3, 12, 21, 0,0,0),
                        datetime.datetime.now(), freq='1D'))
    avondklok_eind = list(pd.date_range(datetime.datetime(2021, 3, 13, 4, 30,0,0),
        datetime.datetime.now(), freq='1D'))
    if len(avondklok_eind) < len(avondklok_begin):
        avondklok_eind.append(datetime.datetime.now())
    avondklok_tijden = zip(avondklok_begin, avondklok_eind)
    for (begin, eind) in avondklok_tijden:
        ax.axvspan(begin, eind, facecolor='k', alpha=0.4)



def punten(plekken, vinders):
    fig, ax = plt.subplots()
    ax.set_title('Puntenverdeling totaal vanaf 12 Maart')
    ax.bar(vinders, [sum(plekken.vinder==vinder) for vinder in vinders])
    ax.grid(axis='y')
    ax.tick_params(axis='x', labelrotation=30)
    fig.savefig(path+'punten')
    plt.close(fig)


def taart(plekken, vinders):
    fig, ax = plt.subplots()
    puntentot = len(plekken)

    ax.set_title('De puntenverdeling, snap je want taartpunten')
    ax.text(0, 0, str(puntentot), fontsize=20, ha='center', va='center')
    # Aantal punten er ook in
    def func(pct, puntentot):
        punten = int(round(pct/100.*puntentot))
        if punten == 1:
            return ""
        return "{:d} pt".format(punten)

    ax.pie([sum(plekken.vinder==vinder) for vinder in vinders], labels=vinders,
           autopct=lambda pct: func(pct, puntentot), pctdistance=0.74)
    fig.savefig(path+'taartpunten')
    plt.close(fig)


def dagen(plekken, dagen):
    fig, ax = plt.subplots()
    ax.set_title('Aantal verdeelde punten per dag')
    ax.set_ylabel('punten')
    ax.bar(dagen, [sum(plekken.vinddag == dag) for dag in dagen])
    ax.grid(axis='y')
    date_ticker(ax, '%a %d-%m')
    ax.tick_params(axis='x', labelrotation=30)
    fig.savefig(path+'dagen')
    plt.close(fig)


def punten_per_dag(plekken, vinders, dagen):
    fig, axs = make_subplots_for(dagen)

    for index, dag in enumerate(dagen):
        ax = axs.flatten()[index]
        ax.axis('on')
        ax.set_ylabel('punten')
        dag_plekken = plekken[plekken.vinddag == dag]
        ax.bar(vinders, [sum(dag_plekken.vinder==vinder) for vinder in vinders])
        ax.set_title(dag.strftime("%A %d-%m"))
        ax.grid(axis='y')
        ax.tick_params(axis='x', labelrotation = 60)
    fig.suptitle('Puntentelling per dag')
    fig.tight_layout()
    fig.savefig(path+'punten_per_dag')
    plt.close(fig)


def punten_per_vinder_per_dag(plekken, vinders, dagen):
    fig, axs = make_subplots_for(vinders)
    for index, vinder in enumerate(vinders):
        ax = axs.flatten()[index]
        ax.set_ylabel('punten')
        ax.axis('on')
        vinder_plekken = plekken[plekken.vinder == vinder]
        ax.bar(dagen, [sum(vinder_plekken.vinddag==dag) for dag in dagen])
        ax.set_xlabel('dagen')
        date_ticker(ax, '%a %d-%m')
        ax.set_title(vinder)
        ax.grid(axis='y')
        ax.tick_params(axis='x', labelrotation =40) 
    fig.suptitle('Op welke dagen was iemand actief')
    fig.tight_layout()
    fig.savefig(path+'punten_per_vinder_per_dag')
    plt.close(fig)


def tijdshistogram(uiterste_tijden, plekken, bins):
    fig, ax = plt.subplots()
    datumrange = pd.date_range(uiterste_tijden[0], uiterste_tijden[1], periods=bins)
    ax.hist(plekken.vindtijd, bins=datumrange)
    ax.tick_params(axis='x', labelrotation =30)
    ax.set_title('punten naar verloop van tijd')
    ax.set_ylabel('hoeveelheid punten in tijdvak')
    ax.set_xlabel('tijdvakken')
    ax.grid(axis='y')
    fig.tight_layout()
    fig.savefig(path+'tijdshistogram')
    plt.close(fig)

def hoe_lang_ligt_de_vlag(plekken, overdag):
    fig, ax = plt.subplots()
    nacht = plekken[plekken.nacht == True]
    ax.scatter(overdag.vindtijd, overdag.vind_duur/60, label='overdag', c='b')
    ax.scatter(nacht.vindtijd, nacht.vind_duur/60, label='tijdens avondklok', c='k')
    ax.set_yscale('log')
    ax.set_yticks([.11667, .25, 1, 3, 10])
    ax.set_yticklabels(['7 min', '15 min', '1 uur', '3 uur', '10 uur'])
    ax.tick_params(axis='x', labelrotation =30) 
    ax.grid(axis='y')
    ax.set_ylabel('minuten (logaritmisch)')
    ax.set_title('De tijd dat ie ergens ligt')
    ax.legend()
    fig.tight_layout()
    fig.savefig(path+'hoe_lang_ligt_de_vlag')
    plt.close(fig)


def hoe_lang_duurt_verstoppen(plekken):
    fig, ax = plt.subplots()
    plekken = plekken[plekken.ketting != 'begin']
    ax.plot(plekken.vindtijd, plekken.verstop_duur)
    ax.set_yscale('linear')
    ax.tick_params(axis='x', labelrotation =30)
    ax.set_title('De tijd voordat ie verstopt is (max 1 uur)')
    ax.set_ylabel('minuten')
    ax.grid(axis='y')
    fig.tight_layout()
    fig.savefig(path+'hoe_lang_duurt_verstoppen')
    plt.close(fig)

def populaire_vindtijden(plekken):
    fig, ax = plt.subplots()
    ax.hist(plekken.hoe_laat_gevonden_m, bins=np.arange(0, 24, 0.5))
    #ax.tick_params(axis='x', labelrotation =20)
    ax.set_ylabel('aantal keer gevonden')
    ax.set_xlabel('tijd op de dag (per half uur)')
    ax.set_xticks([3, 6, 9, 12, 15, 18, 21])
    ax.grid(axis='x')
    ax.axvspan(0, 4.5, facecolor='k', alpha=0.4)
    ax.axvspan(21, 24, facecolor='k', alpha=0.4)
    ax.set_xticklabels(['3:00', '6:00', '9:00', '12:00', '15:00', '18:00', '21:00'])
    ax.set_title('hoe populair is een bepaalde vindtijd?')
    fig.savefig(path+'populaire_vindtijden')
    plt.close(fig)

def gemiddelde_verstoptijd(plekken, verstoppers):
    df = plekken[plekken.ketting != 'begin']
    fig, ax = plt.subplots()
    ax.bar(verstoppers, [df[df.verstopper == verstopper].verstop_duur.mean() for verstopper in verstoppers])
    ax.set_ylabel('minuten')
    ax.set_title('gemiddelde tijd die iemand nodig heeft om de vlag te verstoppen')
    ax.tick_params(axis='x', labelrotation =30)
    fig.savefig(path+'gemiddelde_verstoptijd')
    plt.close(fig)

def gemiddelde_vindtijd(overdag, vinders):
    fig, ax = plt.subplots()
    ax.bar(vinders, [overdag[overdag.vinder==vinder].vind_duur.mean() for vinder in vinders])
    ax.set_ylabel('minuten')
    ax.set_title('gemiddelde tijd die iemand nodig heeft om de vlag te vinden')
    ax.tick_params(axis='x', labelrotation =30)
    fig.savefig(path+'gemiddelde_vindtijd')
    plt.close(fig)

def scatter_vind_verstop(overdag):
    fig, ax = plt.subplots()
    overdag = overdag[overdag.ketting != 'begin']
    ax.scatter(overdag.verstop_duur, overdag.vind_duur)
    ax.set_ylabel('vindtijd in minuten')
    ax.set_xlabel('verstoptijd in minuten (max 60)')
    ax.set_title('Snel verstopt is makkelijk te vinden?')
    fig.savefig(path+'scatter_verstop_v_vind')
    plt.close(fig)

def wiens_vlag_gepakt(plekken, vinders, verstoppers):
    fig, axs = make_subplots_for(vinders)
    for index, vinder in enumerate(vinders):
        ax = axs.flatten()[index]
        ax.set_ylabel('punten')
        ax.axis('on')
        vinder_plekken = plekken[plekken.vinder == vinder]
        anderen = [verstopper for verstopper in verstoppers if verstopper != vinder]
        ax.bar(anderen, [sum(vinder_plekken.verstopper==verstopper) for verstopper in anderen])
        ax.set_title(vinder + ' vond de vlag van')
        ax.tick_params(axis='x', labelrotation =60) 
    #fig.suptitle('Puntentelling per dag')
    fig.tight_layout()
    fig.savefig(path+'wiens_vlag_vond_iedereen')
    plt.close(fig)

def graaf(plekken, vinders):
    #geen idee, was leuk om te proberen, ziet er niet heel cool uit
    G = nx.MultiDiGraph()
    for index, plek in plekken.iterrows():
        G.add_edge(plek['verstopper'], plek['vinder'])
    nx.draw(G)
    plt.title('Dit werkt niet helemaal...')
    plt.savefig(path+'graaf.png')
    plt.close()

def vlagverloop(plekken, vinders):
    fig, ax = plt.subplots(figsize=(15, 5))
    ax.grid(axis='y')
    ax.step(plekken.vindtijd, plekken.vinder, where='post')
    ax.set_title('Wie had de vlag als laatste gevonden op welk moment')
    avondklok_intekenen(ax)
    fig.savefig(path+'vlagverloop')
    plt.close(fig)

def hoe_lang_verstopt_hist(plekken):
    fig, ax = plt.subplots()
    bins = [1, 2, 4, 6, 8, 10, 15, 20, 25, 30, 40, 50, 60, 80, 100, 120, 150, 180,
            210, 240, 300, 360, 480, 600, 12*60, 14*60, 17*60, 20*60, 24*60]

    ax.hist(plekken.vind_duur, bins=bins)
    ax.set_xscale('log')
    ax.set_xticks(bins)
    ax.set_xticklabels([num_to_text(tick) for tick in bins])
    ax.tick_params(axis='x', labelrotation = 90)
    ax.set_xlabel('tijd (logaritimisch)')

    ax.minorticks_off()
    ax.set_title("histogram: hoe lang ligt de vlag verstopt")
    fig.tight_layout()
    fig.savefig(path+"hist_hoe_lang_ligt_de_vlag")

def puntenverdeling_over_tijd(plekken, pts_history, vinders, relatief=True):
    fig, ax = plt.subplots(figsize=(15,8))
    for vinder in vinders:
        if relatief:
            puntlijst = [d[vinder]/sum(d.values()) for d in pts_history]
            ax.step(plekken.vindtijd, puntlijst, label=vinder, where='post')
            hoogst = max(puntlijst)
            tijd = plekken.vindtijd.iloc[puntlijst.index(hoogst)]
            (_, _hoogst) = ax.transData.transform((0, hoogst+0.05))
            ax.text(0, _hoogst, '{}s grootste aandeel, {}%'.format(vinder, hoogst*100))
        if relatief is False:
            puntlijst = [d[vinder] for d in pts_history]
            ax.step(plekken.vindtijd, puntlijst, label=vinder, where='post')
    ax.legend()
    ax.set_ylabel(('Gedeelte van ' if relatief else '') + 'puntentotaal')
    ax.set_xlabel('tijd')
    ax.set_title(('relatieve ' if relatief else '') + 'puntenverdeling over tijd')
    if relatief:
        ax.set_ylim([0,0.4])
    ax.grid(axis='y')
    avondklok_intekenen(ax)
    fig.savefig(path + ('rel_' if relatief else '') + 'puntenverdeling_over_tijd')
    plt.close(fig)

def time2seconds(time):
    return (time.hour * 60 + time.minute) * 60 + time.second

def tijdlijn(plekken, verstoppers):
    fig, ax = plt.subplots(figsize=(15,8))
    start = datetime.date(2021, 3, 12)
    seconds_in_day = 86400
    start_avondklok = 21*60*60
    eind_avondklok  = (4*60+30)*60

    clrs = plt.cm.get_cmap('tab20', len(verstoppers))
    labels = {vinder: Patch(label=vinder, color=clrs(idx)) for idx, vinder in enumerate(verstoppers)}

    for idx, plek in plekken.iterrows():
        row = (plek['verstopdag'] - start).days
        x1 = time2seconds(plek['hoe_laat_verstopt'])
        x2 = time2seconds(plek['hoe_laat_gevonden'])

        if plek['verstopdag'] != plek['vinddag']:
            x2 = seconds_in_day
        
        ax.add_patch(Rectangle((x1, -(row+1)), x2-x1, 1, edgecolor='grey', facecolor=labels[plek['verstopper']].get_fc(), label=plek['verstopper']))
        if plek['ketting'] == 'begin':
            ax.plot([x1, x1], [-row, -(row+1)], linestyle='--', marker='o', linewidth=3, color='#0A0')
        
        if plek['verstopdag'] != plek['vinddag']:
            vind_row = (plek['vinddag'] - start).days
            row += 1
            while row < vind_row:
                ax.add_patch(Rectangle((0, -(row+1)), seconds_in_day, 1, edgecolor='grey', facecolor=labels[plek['verstopper']].get_fc(), label=plek['verstopper']))
                row += 1
            x2 = time2seconds(plek['hoe_laat_gevonden'])
            ax.add_patch(Rectangle((0, -(row+1)), x2, 1, edgecolor='grey', facecolor=labels[plek['verstopper']].get_fc(), label=plek['verstopper']))
        
        if plek['ketting'] == 'eind':
            ax.plot([x2, x2], [-row, -(row+1)], linestyle='--', marker='o', linewidth=3, color='#A00')
    
    plt.yticks(np.flip(np.arange(0, -(row+1), -1)))
    ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda row, x: (start + datetime.timedelta(days=-int(row))).strftime('%d/%m')  )) # time.strftime('%d', start + datetime.timedelta(days=row) )))
    plt.xticks(np.arange(0, seconds_in_day, seconds_in_day/24))
    ax.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda s, x: time.strftime('%H:%M', time.gmtime(s))   ))
    ax.set_xlim(eind_avondklok, start_avondklok)
    ax.set_ylim(-(row+1), 0)
    ax.xaxis.grid(linestyle='dotted')
    ax.set_title("Tijdlijn\nblokjes gekleurd naar wie de vlag verstopt heeft")
    ax.legend(handles = list(labels.values()) + \
        [Line2D([0],[0], linestyle='--', marker='o', linewidth=3, color='#0A0', label='begin'), Line2D([0],[0], linestyle='--', marker='o', linewidth=3, color='#A00', label='einde')],
        bbox_to_anchor=(1, 1), loc='upper left')
    fig.savefig(path+"tijdlijn")
    plt.close(fig)


def gemiddelde_afstand_afgelegd(plekken, verstoppers):
    df = plekken[plekken.ketting != 'begin']
    fig, ax = plt.subplots()
    ax.bar(verstoppers, [df[df.verstopper == verstopper].afstand_afgelegd.mean() for verstopper in verstoppers])
    ax.set_ylabel('meters')
    ax.set_title('gemiddelde afstand die iemand aflegt om de vlag weer te verstoppen')
    ax.tick_params(axis='x', labelrotation =30)
    fig.savefig(path+'gemiddelde_afstand_afgelegd')
    plt.close(fig)

def totale_afstand_afgelegd(plekken, verstoppers):
    df = plekken[plekken.ketting != 'begin']
    fig, ax = plt.subplots()
    ax.bar(verstoppers, [df[df.verstopper == verstopper].afstand_afgelegd.sum() for verstopper in verstoppers])
    ax.set_ylabel('meters')
    ax.set_title('totale afstand die iemand heeft afgelegd om de vlag weer te verstoppen')
    ax.tick_params(axis='x', labelrotation =30)
    ax.grid(axis='y', which='major')
    fig.savefig(path+'totale_afstand_afgelegd')
    plt.close(fig)

def gemiddelde_snelheid(plekken, verstoppers):
    df = plekken[plekken.ketting != 'begin']
    fig, ax = plt.subplots()
    ax.bar(verstoppers, [(df[df.verstopper == verstopper].afstand_afgelegd / df[df.verstopper == verstopper].verstop_duur).mean() /1000*60 for verstopper in verstoppers])
    ax.set_ylabel('km/u')
    ax.set_title('gemiddelde snelheid waarmee iemand de vlag verstopt')
    ax.tick_params(axis='x', labelrotation =30)
    fig.savefig(path+'gemiddelde_snelheid')
    plt.close(fig)

def scatter_verband_verstop_tijd_afstand(plekken):
    df = plekken[plekken.ketting != 'begin']
    fig, ax = plt.subplots()
    ax.scatter(df.afstand_afgelegd, df.verstop_duur)
    ax.set_xlabel('afgelegde afstand vlag (meters)')
    ax.set_ylabel('tijd tussen vondst en nieuwe foto (minuten)')
    ax.set_title('verband tussen afstand tussen plekken en verstoptijd')
    fig.savefig(path+'scatter_afstand_tijd')
    plt.close(fig)
